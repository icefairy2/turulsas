import React from 'react';
import './App.css';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <h1>
          Készül az új oldal!
        </h1>
        <a
          className="App-link"
          href="https://www.facebook.com/turulsas1994/"
          target="_blank"
          rel="noopener noreferrer"
        >
          Turul-Sas Hagyományőrző Egyesület
        </a>
      </header>
    </div>
  );
}

export default App;
