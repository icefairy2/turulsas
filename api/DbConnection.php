<?php

class DbConnection
{

    public static function createConnection(){
      $HOST = getenv("DB_HOST") ?: "localhost";
      $USER = getenv("DB_USER") ?: "turulexb_user";
      $PASSWORD = getenv("DB_PWD") ?: "";
      $DATABASE = getenv("DB_DATABASE") ?: "turulexb_db";
        $mysqli = new mysqli($HOST, $USER, $PASSWORD, $DATABASE);
        $mysqli->set_charset("utf8");
        return $mysqli;
    }
}
